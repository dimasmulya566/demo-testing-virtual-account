package testing.demo.virtual.account.Demo.Testing.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class Transaksi {
    private String transaksiId;
    private String virtualAccountName;
    private String virtualAccountNo;
    private double feeAmount;
    private double amount;
    private Timestamp paidDate;
    private Timestamp closePeriod;
    private Timestamp openPeriod;
    private String kodeBank;
    private String paymentStatus;
    private String nomorHp;

}
