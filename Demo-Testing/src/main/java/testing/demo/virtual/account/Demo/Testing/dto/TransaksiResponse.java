package testing.demo.virtual.account.Demo.Testing.dto;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import testing.demo.virtual.account.Demo.Testing.model.Transaksi;

import java.sql.Timestamp;

@Data
public class TransaksiResponse {
  private Transaksi transaksi;
}
