package testing.demo.virtual.account.Demo.Testing.config;

public class Constants {
    public static final String SWAGGER_TITLE              = "Demo Testing";
    public static final String SWAGGER_DESCRIPTION        = "THIS SWAGGER DOCS FOR DEMO TESTING VIRTUAL ACCOUNT BACKEND";
    public static final String SWAGGER_VERSION            = "v0.0.1-demo-testing";

    public static final String SWAGGER_CONTACT            = "dimasmulya566@gmail.com";
    public static final String SWAGGER_EXT_DESCRIPTION    = "External Documentation";
}
