package testing.demo.virtual.account.Demo.Testing;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@MapperScan("testing.demo.virtual.account.Demo.Testing.mapper")
@ComponentScan({"testing.demo.virtual.account.Demo.Testing.*"})
@EnableScheduling
public class DemoTestingApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoTestingApplication.class, args);
	}

}
