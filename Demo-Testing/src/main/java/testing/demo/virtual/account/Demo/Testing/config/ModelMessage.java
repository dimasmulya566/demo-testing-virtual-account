package testing.demo.virtual.account.Demo.Testing.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ModelMessage {
    private String message;
    private Object data;
    private boolean status;
}
