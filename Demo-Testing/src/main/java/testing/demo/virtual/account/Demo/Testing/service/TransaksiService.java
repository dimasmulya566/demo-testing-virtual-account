package testing.demo.virtual.account.Demo.Testing.service;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import testing.demo.virtual.account.Demo.Testing.dto.TransaksiResponse;
import testing.demo.virtual.account.Demo.Testing.mapper.TransaksiMapper;
import testing.demo.virtual.account.Demo.Testing.model.Transaksi;

import java.sql.Timestamp;
import java.util.*;

@Service
@Slf4j
public class TransaksiService {

    @Autowired
    private TransaksiMapper transaksiMapper;

    public List<Transaksi> findAllTransaksi() {
        return transaksiMapper.findAllTransaksi();
    }

    public Transaksi insert(String virtualAccountName,
                                    double amount,
                                    String kodeBank,
                                    String nomorHp) {
        double feeAmount = 1500;
        double totalAmount = amount + feeAmount;

        Date date = new Date();
        Timestamp openPeriod = new Timestamp(date.getTime());

        Calendar c = Calendar.getInstance();
        c.setTime(date);
//        c.add(Calendar.HOUR, 12);
        c.add(Calendar.MINUTE, 2);
        Timestamp closePeriod = new Timestamp(c.getTimeInMillis());

        String virtualAccountNo = kodeBank + nomorHp;

        Transaksi transaksi = new Transaksi();
        transaksi.setTransaksiId(UUID.randomUUID().toString());
        transaksi.setVirtualAccountName(virtualAccountName);
        transaksi.setVirtualAccountNo(virtualAccountNo);
        transaksi.setFeeAmount(feeAmount);
        transaksi.setAmount(totalAmount);
        transaksi.setPaidDate(null);
        transaksi.setClosePeriod(closePeriod);
        transaksi.setOpenPeriod(openPeriod);
        transaksi.setKodeBank(kodeBank);
        transaksi.setPaymentStatus("0");
        transaksi.setNomorHp(nomorHp);

        transaksiMapper.insertTransaksi(transaksi);
        return transaksi;


    }

    @Scheduled(fixedRate = 120000)
    @SneakyThrows
    public void schedulingPaymentStatus(){
        try {
            List<Transaksi> transaksiList = transaksiMapper.findByPaymentStatus();
            for (Transaksi transaksi : transaksiList) {
                Date closePeriod = transaksi.getOpenPeriod();
                Date currentTime = new Date();
                Timestamp timestamp = new Timestamp(currentTime.getTime());
                Timestamp closeTimestamp = new Timestamp(closePeriod.getTime());
                log.info("VA = " + transaksi.getVirtualAccountNo());
                log.info("Close Period = " + closeTimestamp);

                if (timestamp.after(closeTimestamp)) {
                    transaksiMapper.updatePaymentStatus("2", timestamp, transaksi.getTransaksiId());
                    log.info("Success update payment status");
                }
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }
    }
}
