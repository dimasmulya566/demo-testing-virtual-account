package testing.demo.virtual.account.Demo.Testing.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;

@Configuration
@OpenAPIDefinition
public class SwaggerConfig {
    @Bean
    public OpenAPI springShopOpenAPI() {
        return new OpenAPI()
                .info(new Info().title(Constants.SWAGGER_TITLE)
                        .description(Constants.SWAGGER_DESCRIPTION)
                        .version(Constants.SWAGGER_VERSION))
                .externalDocs(new ExternalDocumentation()
                        .description(Constants.SWAGGER_EXT_DESCRIPTION));
    }

}
