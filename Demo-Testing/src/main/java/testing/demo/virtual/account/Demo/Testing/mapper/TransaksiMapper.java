package testing.demo.virtual.account.Demo.Testing.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import testing.demo.virtual.account.Demo.Testing.dto.TransaksiResponse;
import testing.demo.virtual.account.Demo.Testing.model.Transaksi;

import java.sql.Timestamp;
import java.util.List;

@Mapper
@Repository
public interface TransaksiMapper {
    List<Transaksi> findAllTransaksi();
    void insertTransaksi (Transaksi transaksi);

    List<Transaksi> findByPaymentStatus();

    void updatePaymentStatus(@Param("paymentStatus") String paymentStatus,
                             @Param("closePeriod") Timestamp closePeriod,
                             @Param("transaksiId") String transaksiId);

//    Transaksi findByVirtualAccountNo(@Param("virtualAccountNo") String virtualAccountNo);
}
