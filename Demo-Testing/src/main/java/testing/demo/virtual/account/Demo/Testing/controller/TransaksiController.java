package testing.demo.virtual.account.Demo.Testing.controller;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import testing.demo.virtual.account.Demo.Testing.config.ModelMessage;
import testing.demo.virtual.account.Demo.Testing.dto.TransaksiResponse;
import testing.demo.virtual.account.Demo.Testing.model.Transaksi;
import testing.demo.virtual.account.Demo.Testing.service.TransaksiService;

import java.util.List;

@RestController
@RequestMapping("/demo-testing/virtual-account")
public class TransaksiController {
    @Autowired
    private TransaksiService transaksiService;

    @GetMapping("/transaksi")
    public List<Transaksi> findAllTransaksi() {
        return transaksiService.findAllTransaksi();
    }

    @PostMapping("/insert-transaksi")
    @SneakyThrows
    public ModelMessage insertTransaksi(@RequestParam String virtualAccountName,
                                        @RequestParam double amount,
                                        @RequestParam String kodeBank,
                                        @RequestParam String nomorHp) {
        try {
            ModelMessage modelMessage = new ModelMessage();
            Transaksi responses = transaksiService.insert(virtualAccountName, amount, kodeBank, nomorHp);
            modelMessage.setStatus(true);
            modelMessage.setMessage("Success");
            modelMessage.setData(responses);
            return modelMessage;
        }catch (Exception e) {
            return new ModelMessage(e.getMessage(),null,false );
        }

    }
}
